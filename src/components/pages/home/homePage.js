import React, { Component } from 'react';

import Carousel from './carousel';

class Homepage extends Component {

    render() {
        return(
            <div cassName="container-fuid">
                <Carousel />
            </div>
        );
    }
}

export default Homepage;

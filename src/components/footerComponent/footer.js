import React, { Component } from 'react';
import {
    Link
} from 'react-router-dom'

// Includes
import '../../Assets/footer/css/default_footer.min.css';

class Footer extends Component {
    render() {
        return(
            <footer className="BC-footer">

                <div className="BC-logo">
                    LOGO
                </div>

                <div className="BC-copy">
                    <p>
                        Copyright &copy; 2018 Block Cup, Inc.
                    </p>
                </div>

                <div className="BC-menu">
                    <ul>
                        <li className="BC-first">
                            <Link to="/terms">Terms</Link>
                        </li>
                        <li>
                            <Link to="/privacity-policy">Privacity Policy and Cookie Policy</Link>
                        </li>
                        <li className="BC-last">
                            <Link to="/contact">Intellectual Property</Link>
                        </li>
                    </ul>
                </div>

            </footer>
        );
    }
}

export default Footer;

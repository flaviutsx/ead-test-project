import React, { Component } from 'react';


import {
    Link
} from 'react-router-dom';

// Includes
import '../../Assets/header/css/default_header.min.css';

class Header extends Component {

    render() {

        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };

        return (

            <header className="BC-header">

                <div className="logo">
                    LOGO
                </div>

                <form className="BC-search-form">
                    <input type="search" name="googlesearch" placeholder="Search" />
                </form>

                <nav className="BC-nav">
                    <ul>
                        <li className="BC-first">
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/courses">Courses</Link>
                        </li>
                        <li className="BC-last">
                            <Link to="/contact">Contact</Link>
                        </li>
                    </ul>
                </nav>
                <br />
                <br />
            </header>
        );
    }
}

export default Header;

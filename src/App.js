import React, { Component } from 'react';
import logo from './logo.svg';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

// Components
import Header from './components/headerComponent/header';
import Footer from './components/footerComponent/footer';
import Homepage from './components/pages/home/homePage';
import Coursespage from './components/pages/courses/coursesPage';
import Contactpage from './components/pages/contact/contactPage';

// Includes
import './Assets/general/css/default.min.css';



class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        
          <Header />
            <Route exact path="/" component={Homepage} />
            <Route exact path="/courses" component={Coursespage} />
            <Route exact path="/contact" component={Contactpage} />
          <Footer />
        
      </div>
      </Router>
    );
  }
}

export default App;

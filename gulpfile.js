'use strict';

// Dependencies
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var changed = require('gulp-changed');

//////////////
// - SASS/CSS
//////////////

//General
var SASS_GENERAL_SRC = './src/Assets/general/sass/**/*.sass';
var SASS_GENERAL_DEST = './src/Assets/general/css';

// Header
var SASS_HEADER_SRC = './src/Assets/header/sass/**/*.sass';
var SASS_HEADER_DEST = './src/Assets/header/css';

//Footer
var SASS_FOOTER_SRC = './src/Assets/footer/sass/**/*.sass';
var SASS_FOOTER_DEST = './src/Assets/footer/css';

// Compile SASS for General
gulp.task('compile_sass_general', function(){

    gulp.src(SASS_GENERAL_SRC)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(changed(SASS_GENERAL_DEST))
    .pipe(gulp.dest(SASS_GENERAL_DEST));

});

// Compile SASS for Header
gulp.task('compile_sass_header', function(){

    gulp.src(SASS_HEADER_SRC)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(changed(SASS_HEADER_DEST))
    .pipe(gulp.dest(SASS_HEADER_DEST));

});

// Compile SASS for Footer
gulp.task('compile_sass_footer', function(){

    gulp.src(SASS_FOOTER_SRC)
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(changed(SASS_FOOTER_DEST))
    .pipe(gulp.dest(SASS_FOOTER_DEST));

});

// detect changes in SASS 
gulp.task('watch_sass', function(){
    gulp.watch(SASS_GENERAL_SRC, ['compile_sass_general']);
    gulp.watch(SASS_HEADER_SRC, ['compile_sass_header']);
    gulp.watch(SASS_FOOTER_SRC, ['compile_sass_footer']);
});

//Run tasks
gulp.task('default', ['watch_sass']);
